# Introduction
This project provides a few basic examples of Ansible and Nokia SRLinux interaction 

# 1. Requirements
## 1.1 Install Ansible toolset
- Install ansible
  >Follow official documentation for more details

- Install additional modules, whcih are used in this project
  ```sh
  yum install -y python3-devel
  pip3 install --upgrade pip
  pip3 install --upgrade google-api-python-client
  pip3 install --upgrade setuptools
  pip3 install --upgrade protobuf
  pip3 install --upgrade grpcio
  ```
## 1.2 Install "nokia.grpc"
```sh
ansible-galaxy collection install nokia.grpc
```
>- More information could be found here:
> - https://github.com/nokia/ansible-networking-collections
> - https://galaxy.ansible.com/nokia/grpc

## 1.3 Configure TLS and gNMI Server on SRLinux
- SRLinux configuration exmple: [srlinux.tls.cfg](./srlinux.tls.cfg)

> **Important:** SRLinux certificate should have SAN extension
- Certificate verification example
    ```sh
    # openssl x509 -noout -text -in ixr-6-3.cert.pem | grep "Subject Alternative Name" -A 1
                X509v3 Subject Alternative Name:
                    IP Address:138.203.15.87, DNS:ixr-6-3.ipd.lab
    ```

## 1.4 [Optional] PROXY vs Ansible challenge
- Global proxy configuration (https_proxy and http_proxy in particular) could influence a direct communication between Ansible client and a Target system. In order to mitigate this issue, we should avoid any proxy use, unless it's required by the setup (this option is not covered in this document)
- Options:
  - Unset proxy configuration from OS environment variables
    ```
    unset https_proxy
    unset http_proxy
    ```

# 2. Ansible configuration files

- Important infrastructure files for the project
  - "ansible.cfg"
    ```sh
    [defaults]
    interpreter_python = /usr/bin/python3
    log_path = /tmp/ansible.log
    host_key_checking = False
    hash_behaviour = merge
    retry_files_enabled = False
    library = /usr/share/ansible
    forks = 30

    [persistent_connection]
    connect_timeout = 100

    [ssh_connection]
    retries=1
    ```
  - "hosts"
    ```sh
    [nokia]
    138.203.15.87

    [nokia:vars]
    ansible_network_os=nokia.srlinux
    ansible_root_certificates_file={{ playbook_dir }}/certs/ca.cert.pem
    ansible_port=57400
    ansible_user=admin
    ansible_password=admin
    ansible_connection=nokia.grpc.gnmi
    ansible_gnmi_encoding=JSON_IETF
    #ansible_grpc_channel_options={'grpc.ssl_target_name_override': 'localhost'}
    ansible_grpc_environment={'GRPC_SSL_CIPHER_SUITES': 'AES128', 'HTTPS_PROXY': None}
    ```
  - Certificate files of the trusted Root CA 
    >**Important**: 
      >- Each server or client certificate should have SAN extensions with IP and Hostname defined.
      >- Check "[openssl](https://gitlab.com/Klepikov/openssl)" gitlab project for more information
    ```sh
    # tree certs/
    certs/
    ├── ca.cert.pem
    ```
# 3. Ansible Playbook examples
## Playbook [gNMI_GetConfig.yml](gNMI_GetConfig.yml)
  - Example of getting full or partial configuration
   - In the example below we get full configuration
      ```sh
      # ansible-playbook -i hosts gNMI_GetConfig.yml

      PLAY [Get Configuration using gNMI] *****************************************************************************************************************************************************************

      TASK [Get Full Nodal Configuration (using gNMI GET)] ************************************************************************************************************************************************
      ok: [138.203.15.87]

      TASK [dump test output] *****************************************************************************************************************************************************************************
      ok: [138.203.15.87] => {
          "msg": {
              "srl_nokia-acl:acl": {
                  "cpm-filter": {
                      "ipv4-filter": {
                          "entry": [
                              {
                                  "action": {
                                      "accept": {
                                          "log": false
                                      }
                                  },
                                  "sequence-id": 2
                              },
                              {
                                  "action": {
                                      "accept": {
                                          "log": false
                                      }
                                  },
      <snip>
      ```


## Playbook [gNMI_SubscribeOnce.yml](gNMI_SubscribeOnce.yml)
  - Example of gNMI subscription (mode = ONCE)
    - In the example below we get SW version and Hostname
      ```sh
      # ansible-playbook -i hosts gNMI_SubscribeOnce.yml

      PLAY [gNMI Subscribe ONCE] **************************************************************************************************************************************************************************

      TASK [Get Nodal Configuration (using gNMI Subscribe/ONCE)] ******************************************************************************************************************************************
      ok: [138.203.15.87]

      TASK [dump test output] *****************************************************************************************************************************************************************************
      ok: [138.203.15.87] => {
          "msg": {
              "srl_nokia-platform:platform": {
                  "srl_nokia-platform-control:control": [
                      {
                          "software-version": "v20.6.1-286-g118bc27b34"
                      },
                      {
                          "software-version": "v20.6.1-286-g118bc27b34"
                      }
                  ]
              },
              "srl_nokia-system:system": {
                  "srl_nokia-system-name:name": {
                      "host-name": "IXR-6-3"
                  }
              }
          }
      }

      PLAY RECAP ******************************************************************************************************************************************************************************************
      138.203.15.87              : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
      ```
## Playbook [gNMI_Backup.yml](gNMI_Backup.yml)

  - Example of gNMI backup (save ocnfiguration on Ansible client)

    ```sh
    # ansible-playbook -i hosts gNMI_Backup.yml

    PLAY [NE Backup using gNMI] *************************************************************************************************************************************************************************

    TASK [Get NE Backup] ********************************************************************************************************************************************************************************
    ok: [138.203.15.87]

    TASK [backup details] *******************************************************************************************************************************************************************************
    ok: [138.203.15.87] => {
        "msg": {
            "backup_path": "/tmp/138.203.15.87.config",
            "changed": false,
            "date": "2020-09-17",
            "failed": false,
            "time": "10:06:23"
        }
    }

    PLAY RECAP ******************************************************************************************************************************************************************************************
    138.203.15.87              : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    ```
    - Verification of the backup file
      ```sh
      # cat /tmp/138.203.15.87.config
      {
          "srl_nokia-acl:acl": {
              "cpm-filter": {
                  "ipv4-filter": {
                      "statistics-per-entry": true,
                      "entry": [
                          {
                              "sequence-id": 2,
                              "action": {
                                  "accept": {
                                      "log": false
                                  }
                              }
                          },
                          {
                              "sequence-id": 10,
                              "description": "Accept incoming ICMP unreachable messages",
                              "action": {
                                  "accept": {
                                      "log": false
                                  }
                              },
                              "match": {
                                  "protocol": "icmp",
                                  "icmp": {
                                      "type": "dest-unreachable",
                                      "code": [
                                          0,
                                          1,
                                          2,
                                          3,
                                          4,
                                          13
                                      ]
                                  }
                              }
                          },
      <snip>
      ```

## Playbook [gNMI_Restore.yml](gNMI_Restore.yml)
  - Example of the configuration recovery from a backup file
    ```
    # ansible-playbook -i hosts gNMI_Restore.yml

    PLAY [NE Restore using gNMI] ************************************************************************************************************************************************************************

    TASK [NE Restore (using gNMI SET)] ******************************************************************************************************************************************************************
    ok: [138.203.15.87]

    TASK [dump test output] *****************************************************************************************************************************************************************************
    ok: [138.203.15.87] => {
        "msg": {
            "response": [
                {
                    "op": "REPLACE",
                    "path": ""
                }
            ],
            "timestamp": "2020-09-17T16:13:08.368717"
        }
    }

    PLAY RECAP ******************************************************************************************************************************************************************************************
    138.203.15.87              : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    ```













